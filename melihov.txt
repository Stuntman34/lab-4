{
    "_id": "580dfe098f19e9e3f42d8095",
    "index": 5,
    "guid": "38636f0c-541c-4c44-9010-536f33038b15",
    "isActive": true,
    "balance": "$1,699.56",
    "picture": "http://placehold.it/32x32",
    "age": 37,
    "eyeColor": "brown",
    "name": "step 5",
    "gender": "female",
    "company": "VIXO",
    "email": "elisabethford@vixo.com",
    "phone": "+1 (885) 491-2198",
    "address": "921 Green Street, Durham, Guam, 1243",
    "about": "Ut reprehenderit irure officia et irure consectetur in tempor ipsum aute fugiat duis velit consequat. In incididunt nisi aute incididunt aute occaecat ea labore ut laborum. Non elit eiusmod adipisicing elit velit laboris. Minim duis aute nostrud ea do proident duis officia cillum nostrud est excepteur excepteur. Cupidatat aute irure esse nisi dolor consequat est adipisicing irure elit laborum ut.\r\n",
    "registered": "2014-03-12T06:45:00 -03:00",
    "latitude": -25.407059,
    "longitude": 34.35372,
    "tags": [
      "consectetur",
      "eiusmod",
      "ex",
      "Lorem",
      "elit",
      "reprehenderit",
      "adipisicing"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Kari Grimes"
      },
      {
        "id": 1,
        "name": "Forbes Gibson"
      },
      {
        "id": 2,
        "name": "Krista Durham"
      }
    ],
    "greeting": "Hello, Elisabeth Ford! You have 4 unread messages.",
    "favoriteFruit": "apple"
  }
